

import matplotlib.pyplot as plt
import numpy as np



#%%

m = 100
c = 150
k = 100


#%%

#z_b_dot_dot = - k/m * (z_b_dot - z_e_dot) - c/m * (z_b - z_e)

#%%

t = 0.01
simulation_time = 100

z_b_dot_dot = np.empty([int(10/t)])
z_b_dot = np.empty([int(10/t)])
z_b = np.empty([int(10/t)])
z_e_dot = np.empty([int(10/t)])
z_e = np.empty([int(10/t)])



z_b_dot_dot[0] = 0
z_b_dot[0] = 0
z_b[0] = 0
z_e_dot[0] = 0
z_e[0] = 1

for i in range(1, int(10/t)):
    z_b_dot_dot[i] = - c/m * (z_b_dot[i-1] - z_e_dot[i-1]) - k/m * (z_b[i-1] - z_e[i-1])

    z_b_dot[i] = z_b_dot_dot[i] * t + z_b_dot[i-1]
    z_b[i] = z_b_dot[i]*t + z_b[i-1]
    z_e[i] = z_e_dot[i]*t + z_e[i-1]



ti = range(0, int(10/t))

plt.figure()
plt.plot(ti, z_b)
plt.xlabel("Time [s]")
plt.ylabel("$r$ [rad/s]")
plt.grid(True)
plt.show()


#%%


