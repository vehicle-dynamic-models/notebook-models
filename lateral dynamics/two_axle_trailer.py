import numpy as np
import matplotlib.pyplot as plt


class Results:
    """
    Create a results class to store simulation results.
    """
    def __init__(self, sim_time:int, time_step:float):
        # Time
        sim_length = int(sim_time/time_step)

        self.t              = np.arange(0, sim_length) * time_step #Time [s]

        # Position
        self.x_truck        = np.empty([sim_length]) # Truck's X position in the global frame [m]
        self.y_truck        = np.empty([sim_length]) # Truck's Y position in the global frame [m]
        self.x_trailer      = np.empty([sim_length]) # Trailer's X position in the global frame [m]
        self.y_trailer      = np.empty([sim_length]) # Trailer's Y position in the global frame [m]
        self.corner_radius  = np.empty([sim_length]) # Corner Radius [m]

        # Speed
        self.vx             = np.empty([sim_length]) # Longitudinal velocity at the CG [m/s]
        self.vy             = np.empty([sim_length]) # Lateral velocity at the CG [m/s]
        self.yaw_dot        = np.empty([sim_length]) # Yaw velocity [rad/s]
        self.psi_dot        = np.empty([sim_length]) # Trailer hitch velocity [rad/s]
        self.theta_dot        = np.empty([sim_length]) # Trailer hitch velocity [rad/s]

        # Acceleration
        self.ay             = np.empty([sim_length]) # Truck's lateral acceleration at the CG [m/s2]
        self.yaw_dot_dot    = np.empty([sim_length]) # Truck's yaw acceleration [rad/s^2]
        self.psi_dot_dot    = np.empty([sim_length]) # Trailer's hitch acceleration [rad/s^2]
        self.theta_dot_dot  = np.empty([sim_length])

        # Angle
        self.slip_angle_1   = np.empty([sim_length]) # Truck's front wheel slip angle [rad]
        self.slip_angle_2   = np.empty([sim_length]) # Truck's rear wheel slip angle [rad]
        self.slip_angle_3   = np.empty([sim_length]) # Trailer's wheel slip angle [rad]
        self.psi            = np.empty([sim_length]) # Trailer's hitch angle [rad]
        self.yaw            = np.empty([sim_length]) # Trailer's hitch angle [rad]
        self.theta            = np.empty([sim_length]) # Trailer's hitch angle [rad]

        # Force
        self.Fy_1           = np.empty([sim_length]) # Truck's front wheel lateral force [N]
        self.Fy_2           = np.empty([sim_length]) # Truck's rear wheel lateral force [N]
        self.Fy_3           = np.empty([sim_length]) # Trailer's wheel lateral force [N]
        self.psi_psi           = np.empty([sim_length]) # Trailer's wheel lateral force [N]



class OneTrack:
    def __init__(self):
        # The values were obtained from https://confluence.outrider.ai/display/SIT/Vehicle+properties
        self.Izz        = 16840     # Truck's Inertia in yaw axis [Kg.m^2]
        self.m          = 8600      # Truck's total weight [Kg]
        self.C_alpha_1  = 197705    # Truck's front wheel cornering stiffness [N/rad]
        self.C_alpha_2  = 197705    # Truck's rear wheel cornering stiffness [N/rad]
        self.WD         = 60        # Truck's weight distribution [%]
        self.wheelbase  = 3.1       # Truck's wheelbase [m]

        # Trailer Properties
        self.mc         = 15000     # Trailer's total weight [Kg]
        self.Ic         = 10000     # Trailer's inertia [Kg.m^2]
        self.C_alpha_3  = 243294    # Trailer's wheel cornering stiffness [N/rad]
        self.g          = 12.5      # Trailer's wheelbase [m]
        self.WDc        = 50        # Trailer' weight distribution%

        # Calculations
        self.a1 = (1-self.WD/100) * self.wheelbase  # Distance from truck's CG to front axle [m]
        self.a2 = self.WD/100 * self.wheelbase      # Distance from truck's CG to rear axle [m]
        self.h = self.WD/100 * self.wheelbase       # Distance from hitch to truck's CG [m]
        self.f = (1-self.WDc/100) * self.g            # Distance from trailer's CG to hitch [m]

    def get_center_mass(self):
        cm_location = (self.mc * (self.f + self.h + self.a1) + (self.m * self.a1)) / (self.m + self.mc)
        cm_as_percentage = cm_location / (self.wheelbase + self. g) * 100
        return cm_as_percentage


def run_simulation(vx_i, steer_i, one_track: OneTrack, t = 0.004, sim_time = 200):
    """
    Simulates the vehicle model
    :param vx_i: Longitudinal velocity of the truck in m/s
    :param steer_i: Steering input in rad
    :param one_track: Truck model
    :param t: Simulation time step [s]
    :param sim_time: Simulation time [s]
    :return: Results class
    """

    # Pass vehicle properties to local variable
    Izz = one_track.Izz
    m = one_track.m*9.81
    C_alpha_1 = one_track.C_alpha_1
    C_alpha_2 = one_track.C_alpha_2
    C_alpha_3 = one_track.C_alpha_3
    g = one_track.g
    mc = one_track.mc*9.81
    Ic = one_track.Ic


    # Calculations
    a1 = (1 - one_track.WD / 100) * one_track.wheelbase  # Distance from truck's CG to front axle [m]
    a2 = one_track.WD / 100 * one_track.wheelbase  # Distance from truck's CG to rear axle [m]
    h = one_track.WD / 100 * one_track.wheelbase  # Distance from hitch to truck's CG [m]
    f = (one_track.WDc / 100) * one_track.g  # Distance from trailer's CG to hitch [m]

    one_track.a1 = a1
    one_track.a2 = a2
    one_track.f = f
    one_track.h = h

    ####################################################################################################################
    # Define Input
    steering_at_wheel = np.ones([int(sim_time/t)])*steer_i
    #steering_at_wheel = np.concatenate((np.ones([int(sim_time/2/t)])*0, np.ones([int(sim_time/2/t)])*steer_i))

    ####################################################################################################################
    # Initialize simulation arrays. This will be during the simulation to save the vehicle state
    res = Results(sim_time,t)

    ####################################################################################################################
    # Simulation Initial Conditions
    vx = vx_i
    vy = 0
    yaw_dot = 0
    psi = 0
    psi_dot = 0
    psi_dot_dot = 0
    yaw_dot_dot = 0

    ####################################################################################################################
    # Simulation
    x_truck = 0
    y_truck = 0
    x_trailer = 0
    y_trailer = 0
    yaw0 = 0
    psi0 = 0
    theta_dot0 = 0
    theta0 = 0
    for i in range(1, int(sim_time/t+1)):
        ay_err = 0
        iter = 1000


        while True:

            slip_angle_1 = (steering_at_wheel[i-1] - (vy + a1*yaw_dot)/vx)


            slip_angle_2 = ((a2*yaw_dot - vy)/vx)

            slip_angle_3 = -((vy - h*yaw_dot - g*(yaw_dot - psi_dot))/vx + psi)

            Fy_1 = C_alpha_1 * slip_angle_1
            Fy_2 = C_alpha_2 * slip_angle_2
            Fy_3 = C_alpha_3 * slip_angle_3


            ay = (Fy_1 + Fy_2 + Fy_3 + mc*((h+f)*yaw_dot_dot - f*psi_dot_dot))/(m+mc) - vx*yaw_dot
            yaw_dot_dot = (a1*Fy_1 - a2*Fy_2 - h*Fy_3 + mc*h*(ay + vx*yaw_dot + f*psi_dot_dot))/(Izz+mc*h*(h+f))
            psi_dot_dot = (g*Fy_3 - mc*f*(ay + vx*yaw_dot - h*yaw_dot_dot))/(Ic + mc*f*f) + yaw_dot_dot


            vy = ay*t+vy
            yaw_dot = yaw_dot_dot*t + yaw_dot
            psi_dot = psi_dot_dot*t + psi_dot
            psi = psi_dot*t + psi

            res.vy[i-1] = vy


            res.ay[i-1] = ay + vx*yaw_dot

            res.yaw_dot_dot[i-1] = yaw_dot_dot
            res.yaw_dot[i-1] = yaw_dot

            res.slip_angle_1[i-1] = slip_angle_1
            res.slip_angle_2[i-1] = slip_angle_2
            res.slip_angle_3[i-1] = slip_angle_3

            res.Fy_1[i-1] = Fy_1
            res.Fy_2[i-1] = Fy_2
            res.Fy_3[i-1] = Fy_3

            res.psi[i-1] = psi
            res.psi_dot[i-1] = psi_dot
            res.psi_dot_dot[i-1] = psi_dot_dot



            yaw = yaw_dot*t +  yaw0
            yaw0 = yaw
            psi_psi = psi*t + psi0
            psi0 = psi_psi



            res.x_truck[i-1] = (vx*np.cos(yaw) - vy*np.sin(yaw))*t + x_truck
            res.y_truck[i-1] = (vx*np.sin(yaw) + vy*np.cos(yaw))*t + y_truck

            res.x_trailer[i-1] = (vx*np.cos(psi+yaw) - vy*np.sin(psi+yaw))*t + x_trailer
            res.y_trailer[i-1] = (vx*np.sin(psi+yaw) + vy*np.cos(psi+yaw))*t + y_trailer

            #print("x-truck: " + str(res.y_truck[i-1]) +" " + "x-trailer: " + str(res.y_trailer[i-1]))

            x_truck = res.x_truck[i-1]
            y_truck = res.y_truck[i-1]
            x_trailer = res.x_trailer[i-1]
            y_trailer = res.y_trailer[i-1]
            res.yaw[i-1] = yaw
            res.psi_psi[i-1] = psi_psi

            res.theta_dot_dot[i-1] = psi_dot_dot - yaw_dot_dot
            res.theta_dot[i-1] = res.theta_dot_dot[i-1]*t + theta_dot0
            res.theta[i-1] = res.theta_dot[i-1]*t + theta0

            theta_dot0 = res.theta_dot[i-1]
            theta0 = res.theta[i-1]


            if abs(ay-ay_err) < 0.001:
                break
            else:
                iter -=1
            ay_err=ay

            if iter == 0:
                return Results(sim_time,t)



    return res


#
vx = 1
st=np.deg2rad(5)
ot = OneTrack()
res = run_simulation(vx,st,ot)

#
# plt.figure()
# plt.plot(res.t, res.ay)
# plt.plot(res.t, res.yaw_dot*57)
# plt.plot(res.t, res.psi*57)
# plt.xlabel("Time [s]")
# plt.ylabel("$r$ [deg/s]")
# plt.legend(["Lateral Acceleration ay [m/s2]","Yaw velocity [deg/s]","Trailer Angle [deg]"])
# plt.grid(True)
#
# plt.figure()
# plt.plot(res.t, res.vy)
# plt.plot(res.t, res.ay)
# plt.xlabel("Time [s]")
# plt.ylabel("$v_y$, $a_y$ [m/s, m/s2]")
# plt.legend(["Lateral velocity vy [m/s]","Lateral acceleration [m/s2]"])
# plt.grid(True)
#
# plt.figure()
# plt.plot(res.t, res.slip_angle_1*57)
# plt.plot(res.t, res.slip_angle_2*57)
# plt.plot(res.t, res.slip_angle_3*57)
# plt.xlabel("Time [s]")
# plt.ylabel("Slip angle [deg]")
# plt.legend(["Steer axle","Drive axle", "Trailer axle"])
# plt.grid(True)
#
# plt.figure()
# plt.plot(res.t, res.Fy_1)
# plt.plot(res.t, res.Fy_2)
# plt.plot(res.t, res.Fy_3)
# plt.xlabel("Time [s]")
# plt.ylabel("Lateral force [N]")
# plt.legend(["Steer axle","Drive axle", "Trailer axle"])
# plt.grid(True)
#
# plt.figure()
# plt.plot(res.t, res.psi*57)
# plt.plot(res.t, res.psi_dot*57)
# plt.plot(res.t, res.psi_dot_dot*57)
# plt.xlabel("Time [s]")
# plt.ylabel("Trailer angle [deg]")
# plt.legend(["Angle","Angle speed", "Angle acceleration"])
# plt.grid(True)
#
#
# plt.figure()
# plt.plot(res.t, res.yaw_dot*57)
# plt.plot(res.t, res.yaw_dot_dot*57)
# plt.xlabel("Time [s]")
# plt.ylabel("Truck angle [deg]")
# plt.legend(["Angle speed", "Angle acceleration"])
# plt.grid(True)
#
# plt.figure()
# plt.plot(res.t[10:], vx/res.yaw_dot[10:])
# plt.plot(res.t[10:], vx/res.psi[10:])
# plt.xlabel("Time [s]")
# plt.ylabel("Turning radius [m]")
# plt.legend(["Truck", "Trailer"])
# plt.grid(True)
#
# print("Speed: ", vx)
# print("Truck: ", vx/res.yaw_dot[-1])
# print("Trailer: ", vx/res.psi[-1])
#
# plt.figure()
# plt.plot(res.x_truck, res.y_truck)
# plt.plot(res.x_trailer, res.y_trailer)
# plt.xlabel("X position [m]")
# plt.ylabel("Y position [m]")
# plt.legend(["Truck", "Trailer"])
# plt.grid(True)
#
# plt.show()

# plt.figure()
# plt.plot(res.t, np.rad2deg(res.yaw_dot))
# plt.plot(res.t, np.rad2deg(res.psi))
# #plt.plot(res.t, np.rad2deg(res.psi - res.yaw))
# plt.xlabel("X position [m]")
# plt.ylabel("Y position [m]")
# plt.legend(["Yaw_dot", "Psi_dot","Theta_dot"])
# plt.grid(True)
#
# plt.show()


# The following section prints a simulation batch run


print(      '{:^15} | {:^15} | {:^15} | {:^15} | {:^15} | {:^15} | {:^15}  | {:^15}| {:^15}| {:^15}| {:^15}| {:^15}| {:^15}| {:^15} '
      .format("Steering (deg)","Vx (m/s)", "Ay (m/s2)",     "r (deg/s)",      "psi (deg/s)", 'T. Radius1 (m)', 'T. Radius2 (m)'  ,"SA1 (deg)",   "SA2 (deg)",   "SA3 (deg)",   "Fy1 (N)",   "Fy2 (N)",   "Fy3 (N)", 'Theta (deg)'))

print("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
for speed_input in range(1,2,1):
    for steer_input in np.arange(14, 18 ,0.1):
        vx = speed_input
        st=np.deg2rad(steer_input)
        ot = OneTrack()


        res = run_simulation(vx,st,ot)


        print('{:^15.2f} | {:^15.1f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f}| {:^15.2f}| {:^15.2f}| {:^15.2f}| {:^15.2f}| {:^15.2f}'
              .format(np.rad2deg(st),
                      vx,
                      res.ay[-1],
                      np.rad2deg(res.yaw_dot[-1]),
                      np.rad2deg(res.psi[-1]),
                      vx/res.yaw_dot[-1],
                      vx/(res.psi[-1] - res.yaw_dot[-1]) ,
                      np.rad2deg(res.slip_angle_1[-1]),
                      np.rad2deg(res.slip_angle_2[-1]),
                      np.rad2deg(res.slip_angle_3[-1]),
                      res.Fy_1[-1],
                      res.Fy_2[-1],
                      res.Fy_3[-1],
                      np.rad2deg(res.psi[-1])))

        #a = res.Fy_1[-1] * ot.a1 - res.Fy_2[-1] * ot.a2 + res.Fy_3[-1] * (ot.g +ot.h)
        #print(ot.get_center_mass()/100*(ot.g+ot.wheelbase))



# Print for each time step
# print(      '{:^15} | {:^15} | {:^15} | {:^15} | {:^15} | {:^15} | {:^15} | {:^15}  | {:^15}| {:^15}| {:^15}| {:^15}| {:^15}| {:^15} '
#       .format("Time (s)", "Steering (deg)","Vx (m/s)", "Ay (m/s2)",     "r (deg/s)",      "psi (deg)", 'T. Radius1 (m)', 'T. Radius2 (m)'  ,"SA1 (deg)",   "SA2 (deg)",   "SA3 (deg)",   "psi_d (deg/s^2)",   "psi_dd (deg/s^3)",   "Fy3 (N)"))
#
# print("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
#
# vx = 2
# st=np.deg2rad(1)
# ot = OneTrack()
#
# sim_t = 200
# res = run_simulation(vx,st,ot,sim_time=sim_t)
#
# for i in range(0,len(res.t),sim_t):
#
#     print(
#         '{:^15.2f} | {:^15.2f} | {:^15.1f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f} | {:^15.2f}| {:^15.2f}| {:^15.2f}| {:^15.2f}| {:^15.2f} '
#         .format(res.t[i],
#                 np.rad2deg(st),
#                 vx,
#                 res.ay[i],
#                 np.rad2deg(res.yaw_dot[i]),
#                 np.rad2deg(res.psi[i]),
#                 vx / res.yaw_dot[i],
#                 -vx / (res.psi_dot[i] - res.yaw_dot[i]),
#                 np.rad2deg(res.slip_angle_1[i]),
#                 np.rad2deg(res.slip_angle_2[i]),
#                 np.rad2deg(res.slip_angle_3[i]),
#                 np.rad2deg(res.psi_dot[i]),
#                 np.rad2deg(res.psi_dot_dot[i]),
#                 res.Fy_3[i]))

#
# for speed_input in range(1,6,1):
#     for steer_input in range(1,2 ,1):
#         for trailer_weight in range(2500, 22500, 2500):
#             vx = speed_input
#             st=np.deg2rad(steer_input)
#             ot = OneTrack()
#             res = run_simulation(vx,st,ot)
#             print('{:^15.2f} | {:^15.2f} | {:^15.2f}'.format(trailer_weight, vx / res.yaw_dot[-1], ot.get_center_mass()))
